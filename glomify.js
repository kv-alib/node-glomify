var crypto = require('crypto'),
  uglifyjs = require('uglify-js'),
  uglifycss = require('uglifycss'),
  fs = require('fs'),
  _path = require('path');

// run once on app startup to generate mangled and minified scripts and css. Example:
// 
// require('glomify.js')(app,{
//   js: {
//     my: [
//       'js/big1/js',
//       'js/big2/js'
//     ]
//   },
//   css: {
//     ltr_small: [
//       'css/ltr_small1.css', 'css/ltr_small2.css'
//     ],
//     ltr_large: [
//       'css/ltr_large1.css', 'css/ltr_large2.css'
//     ],
//     rtl_small: [
//       'css/rtl_small1.css', 'css/rtl_small2.css',
//     ],
//     rtl_large: [
//       'css/rtl_large1.css', 'css/rtl_large2.css'
//     ]
//   },
//   path.join(__dirname,'public')
// );
//
// will add a resources object to res.locals for all
// requests, where resources follows the structure of
// the json passed to require, allowing you to write
// script tags like this:
//
//   <script src="{{resources.js.my}}"></script>
// 
// and css link tags like this:
//
//   <link rel="stylesheet" href="{{resources.css.ltr_small}}"/>

var endsWith = function(str, suffix) {
  return str.indexOf(suffix, str.length - suffix.length) !== -1;
};

var isGlomFile = function(str) {
  return endsWith(str, '.glom.js') || endsWith(str, '.glom.css');
};

module.exports = {

  setup: function(statics, options) {

    var resources = {};
    resources.css = {};
    resources.js = {};
    
    console.warn('glomify is running in ' + options.mode + ' mode');

    for (var type in statics) {
      var res = statics[type];
      for (var glom in res) {
        var components = res[glom];
        this.setupProduction(resources, type, glom, components, options);
      }
    }

    return function(req, res, next) {
      // add the resources to res.locals(), so that they are available
      // directly to mustache templates simply by {{resources}}
      res.locals.resources = resources;

      // permanently cache glommed files
      if (isGlomFile(req.url)) {
        res.setHeader('Cache-Control', 'max-age=31556926'); // one year 
        res.setHeader('Vary', 'Accept-Encoding'); // allow proxies to cache compressed + uncompressed variants
      }

      // and let the next middleware handle...
      next();
    };
  },

  setupProduction: function(resources, type, glom, components, options) {
    var files = [];
    for (var i = 0; i < components.length; i++) {
      console.log("glomming " + components[i]);
      files.push(components[i]);
    }
    var text = "";
    if ('css' == type) {
      text = uglifycss.processFiles(files);
      resources.css[glom] = this.add(resources.css[glom], this.store(text, type, options.path, glom));
    } else {
      text = uglifyjs.minify(files).code;
      resources.js[glom] = this.add(resources.js[glom], this.store(text, type, options.path, glom));
    }
  },

  add: function(list, file) {
    if (!list) list = [];
    list.push({
      'name': file
    });
    return list;
  },

  store: function(text, type, root, name, original_name) {
    var hash = this.md5sum(text);
    var path = _path.join(root, type);
    if (!fs.existsSync(path)) {
      if (!fs.existsSync(root)) fs.mkdirSync(root);
      fs.mkdirSync(path);
    }
    var filename = name + "." + hash + ".glom." + type;
    if (original_name) filename = name + "." + hash + "." + original_name;
    
    fs.writeFileSync(_path.join(path, filename), text);
    return "/" + _path.join(type, filename);
  },
  
  md5sum: function(text) {
    var md5 = crypto.createHash('md5');
    md5.update(text);
    return md5.digest('hex').slice(0, 10);
  }

};
